package com.catonetwork.accesscontrol.service;

import com.catonetwork.accesscontrol.servicemodel.Permissions;
import java.util.List;

public interface IdentitiesService {

  List<Permissions> getUserEndpoints(String userId, String pass) throws Exception;

}
