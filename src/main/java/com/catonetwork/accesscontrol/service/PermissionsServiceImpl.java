package com.catonetwork.accesscontrol.service;

import com.catonetwork.accesscontrol.repository.PermissionsRepository;
import com.catonetwork.accesscontrol.repository.PermissionsRolesMapRepository;
import com.catonetwork.accesscontrol.servicemodel.Identities;
import com.catonetwork.accesscontrol.servicemodel.Permissions;
import com.catonetwork.accesscontrol.servicemodel.Roles;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionsServiceImpl implements PermissionsService {

  @Autowired private RolesService rolesService;

  @Autowired private PermissionsRepository permissionsRepository;

  @Autowired private PermissionsRolesMapRepository permissionsRolesMapRepository;

  @Override
  public List<Permissions> getPermissions(Identities userId) {
    List<Roles> userRoles = rolesService.getUserRole(userId);
    return permissionsRolesMapRepository.findByRolesIn(userRoles).stream()
        .map(permissionsRolesMap -> permissionsRolesMap.getPermissions())
        .collect(Collectors.toList());
  }
}
