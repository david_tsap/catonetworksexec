package com.catonetwork.accesscontrol.service;

import com.catonetwork.accesscontrol.repository.IdentitiesRolesMapRepository;
import com.catonetwork.accesscontrol.repository.RolesRepository;
import com.catonetwork.accesscontrol.servicemodel.Identities;
import com.catonetwork.accesscontrol.servicemodel.IdentitiesRolesMap;
import com.catonetwork.accesscontrol.servicemodel.Roles;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolesServiceImpl implements RolesService{

  @Autowired
  private RolesRepository rolesRepository;

  @Autowired
  private IdentitiesRolesMapRepository identitiesRolesMapRepository;

  @Override
  public List<Roles> getUserRole(Identities identities) {
    List<IdentitiesRolesMap> identitiesRolesMaps = identitiesRolesMapRepository
        .findByIdentities(identities);
    return identitiesRolesMaps.stream().map(IdentitiesRolesMap::getRoles).collect(
        Collectors.toList());
  }
}
