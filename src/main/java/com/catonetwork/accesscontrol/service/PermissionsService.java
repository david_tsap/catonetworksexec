package com.catonetwork.accesscontrol.service;

import com.catonetwork.accesscontrol.servicemodel.Identities;
import com.catonetwork.accesscontrol.servicemodel.Permissions;
import java.util.List;

public interface PermissionsService {

  List<Permissions> getPermissions(Identities userId);
}
