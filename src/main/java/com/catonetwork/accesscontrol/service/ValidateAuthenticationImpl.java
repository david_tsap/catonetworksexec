package com.catonetwork.accesscontrol.service;

import com.catonetwork.accesscontrol.globalconfigurations.GlobalConfigurationsService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidateAuthenticationImpl implements ValidateAuthentication{

  @Autowired
  private GlobalConfigurationsService globalConfigurationsService;

  public boolean validateUser(String userId,String pass){
    Map<String, String> authorization = globalConfigurationsService.getAuthorization()
        .getAuthorization();
    if(authorization.containsKey(userId) && authorization.get(userId).equals(pass)){
      return true;
    }
    return false;
  }
}
