package com.catonetwork.accesscontrol.service;

import com.catonetwork.accesscontrol.globalconfigurations.GlobalConfigurationsService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public interface ValidateAuthentication {


  public boolean validateUser(String userId,String pass);
}
