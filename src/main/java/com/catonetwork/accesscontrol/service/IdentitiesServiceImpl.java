package com.catonetwork.accesscontrol.service;

import com.catonetwork.accesscontrol.exception.ErrorCodeInterface;
import com.catonetwork.accesscontrol.exception.Errors;
import com.catonetwork.accesscontrol.exception.IdentityException;
import com.catonetwork.accesscontrol.repository.IdentitiesRepository;
import com.catonetwork.accesscontrol.servicemodel.Identities;
import com.catonetwork.accesscontrol.servicemodel.Permissions;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IdentitiesServiceImpl implements IdentitiesService {


  @Autowired
  private ValidateAuthenticationImpl validateAuthenticationImpl;

  @Autowired
  private PermissionsService permissionsService;

  @Autowired
  private IdentitiesRepository identitiesRepository;

  @Override
  public List<Permissions> getUserEndpoints(String userId, String pass) throws Exception {
    if(!validateAuthenticationImpl.validateUser(userId,pass)){
     throw new IdentityException("unauthorized user").setErrorCode(Errors.IDN_10000);
    }
    List<Identities> byUserId = identitiesRepository.findByUserId(userId);
    if (!byUserId.isEmpty()) {
      return permissionsService.getPermissions(byUserId.get(0));
    }else{
      throw new IdentityException("unauthorized user").setErrorCode(Errors.IDN_10000);
    }
  }
}
