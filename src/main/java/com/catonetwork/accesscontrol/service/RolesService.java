package com.catonetwork.accesscontrol.service;

import com.catonetwork.accesscontrol.servicemodel.Identities;
import com.catonetwork.accesscontrol.servicemodel.Roles;
import java.util.List;

public interface RolesService {

  List<Roles> getUserRole(Identities identities);
}
