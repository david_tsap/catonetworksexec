package com.catonetwork.accesscontrol.exception;


public enum Errors implements ErrorCodeInterface {
  IDN_10000("IDN-10000", "Unauthorized user");

  private String code;
  private String message;

  private Errors(String code, String message) {
    this.code = code;
    this.message = message;
  }

  @Override
  public String code() {
    return this.code;
  }

  @Override
  public String message() {
    return this.message;
  }

  @Override
  public String toString() {
    return "Errors{" +
        "code='" + code + '\'' +
        ", message='" + message + '\'' +
        '}';
  }
}
