package com.catonetwork.accesscontrol.exception;

public interface ErrorCodeInterface {

  String code();

  String message();
}
