package com.catonetwork.accesscontrol.exception;

public class IdentityException extends RuntimeException{

  protected ErrorCodeInterface errorCode;

  public IdentityException(String massage) {
    super(massage);
  }

  public IdentityException setErrorCode(ErrorCodeInterface errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  public ErrorCodeInterface getErrorCode() {
    return errorCode;
  }


}
