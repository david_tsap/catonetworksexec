package com.catonetwork.accesscontrol.globalconfigurations;

public class GlobalConfigurations {

  private Authorization authorization;

  public Authorization getAuthorization() {
    return authorization;
  }

  public void setAuthorization(
      Authorization authorization) {
    this.authorization = authorization;
  }
}
