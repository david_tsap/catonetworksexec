package com.catonetwork.accesscontrol.globalconfigurations;


public interface GlobalConfigurationsService {

    Authorization getAuthorization();

}