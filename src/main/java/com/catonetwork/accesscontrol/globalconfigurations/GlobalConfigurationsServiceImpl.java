package com.catonetwork.accesscontrol.globalconfigurations;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GlobalConfigurationsServiceImpl implements GlobalConfigurationsService {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private ObjectMapper OBJECT_MAPPER;
    private Authorization authorization;

    private GlobalConfigurations globalConfigurations = new GlobalConfigurations();

    private static final String DIRECTORY_NAME = "/static/";
    private static final String CONFIGURATION_DATA = "configuration.json";

    @PostConstruct
    void load() throws IOException {
        this.OBJECT_MAPPER = new ObjectMapper();
        this.OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        this.loadGlobalConfigurationsFromFiles();
    }

    private void loadGlobalConfigurationsFromFiles() throws IOException {

        load(CONFIGURATION_DATA, (inputFile) -> {
            authorization = this.OBJECT_MAPPER.readValue(inputFile,
                new TypeReference<Authorization>() {
                });
            globalConfigurations.setAuthorization(authorization);
        });
    }

    private void load(String fileName, CustomConsumer<InputStream> consumer) throws IOException {
        InputStream inputFile = this.getClass().getResourceAsStream(DIRECTORY_NAME + fileName);
        consumer.accept(inputFile);
    }



    @Override
    public Authorization getAuthorization() {
        return authorization;
    }

    @FunctionalInterface
    public interface CustomConsumer<T> {

        void accept(T t) throws IOException;
    }

}