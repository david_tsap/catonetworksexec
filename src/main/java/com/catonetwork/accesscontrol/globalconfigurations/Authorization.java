package com.catonetwork.accesscontrol.globalconfigurations;

import java.util.Map;

public class Authorization {

  public Map<String, String> getAuthorization() {
    return authorization;
  }

  public void setAuthorization(Map<String, String> authorization) {
    this.authorization = authorization;
  }

  Map<String,String> authorization;

}
