package com.catonetwork.accesscontrol.servicemodel;

import com.catonetwork.accesscontrol.converter.StringListConverter;
import java.util.List;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Roles {

  @Id
  @GeneratedValue
  private Long id;

  private String name;

  @Convert(converter = StringListConverter.class)
  private List<String> subRolesId;

  @OneToMany(mappedBy = "roles")
  private List<IdentitiesRolesMap> identities;

  @OneToMany(mappedBy = "roles")
  private List<PermissionsRolesMap> permissions;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getSubRolesId() {
    return subRolesId;
  }

  public void setSubRolesId(List<String> subRolesId) {
    this.subRolesId = subRolesId;
  }

  public List<IdentitiesRolesMap> getIdentities() {
    return identities;
  }

  public void setIdentities(
      List<IdentitiesRolesMap> identities) {
    this.identities = identities;
  }

  public List<PermissionsRolesMap> getPermissions() {
    return permissions;
  }

  public void setPermissions(
      List<PermissionsRolesMap> permissions) {
    this.permissions = permissions;
  }
}
