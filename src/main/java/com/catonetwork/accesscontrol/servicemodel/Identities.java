package com.catonetwork.accesscontrol.servicemodel;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Identities {

  @Id
  @GeneratedValue
  private Long id;

  @OneToMany(mappedBy = "identities")
  private List<IdentitiesRolesMap> roles;

  private String userId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public List<IdentitiesRolesMap> getRoles() {
    return roles;
  }

  public void setRoles(List<IdentitiesRolesMap> roles) {
    this.roles = roles;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
