package com.catonetwork.accesscontrol.servicemodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class IdentitiesRolesMap {

  @Id
  @GeneratedValue
  private Long id;

  @ManyToOne
  @JoinColumn(name = "identities_id")
  private Identities identities;

  @ManyToOne
  @JoinColumn(name = "roles_id")
  private Roles roles;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Identities getIdentities() {
    return identities;
  }

  public void setIdentities(Identities identities) {
    this.identities = identities;
  }

  public Roles getRoles() {
    return roles;
  }

  public void setRoles(Roles roles) {
    this.roles = roles;
  }
}
