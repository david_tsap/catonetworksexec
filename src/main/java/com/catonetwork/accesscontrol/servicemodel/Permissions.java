package com.catonetwork.accesscontrol.servicemodel;

import java.util.Collection;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity
public class Permissions {

  @Id
  @GeneratedValue
  private Long id;

  private String endpoint;

  @OneToMany(mappedBy = "permissions")
  private List<PermissionsRolesMap> roles;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEndpoint() {
    return endpoint;
  }

  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

  public List<PermissionsRolesMap> getRoles() {
    return roles;
  }

  public void setRoles(
      List<PermissionsRolesMap> roles) {
    this.roles = roles;
  }
}
