package com.catonetwork.accesscontrol.repository;

import com.catonetwork.accesscontrol.servicemodel.Permissions;
import com.catonetwork.accesscontrol.servicemodel.PermissionsRolesMap;
import com.catonetwork.accesscontrol.servicemodel.Roles;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionsRolesMapRepository extends JpaRepository<PermissionsRolesMap,Long> {
  List<PermissionsRolesMap> findByRolesIn(List<Roles> roles);
}
