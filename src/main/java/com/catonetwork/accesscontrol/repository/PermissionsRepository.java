package com.catonetwork.accesscontrol.repository;

import com.catonetwork.accesscontrol.servicemodel.Permissions;
import com.catonetwork.accesscontrol.servicemodel.Roles;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionsRepository extends JpaRepository<Permissions,Long> {

//  List<Permissions> findByRolesIn(List<Roles> roles);

}
