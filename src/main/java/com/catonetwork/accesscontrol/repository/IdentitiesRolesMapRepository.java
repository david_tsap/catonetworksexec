package com.catonetwork.accesscontrol.repository;

import com.catonetwork.accesscontrol.servicemodel.Identities;
import com.catonetwork.accesscontrol.servicemodel.IdentitiesRolesMap;
import com.catonetwork.accesscontrol.servicemodel.Roles;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdentitiesRolesMapRepository extends JpaRepository<IdentitiesRolesMap,Long> {

  List<IdentitiesRolesMap> findByIdentities(Identities identities);

}
