package com.catonetwork.accesscontrol.repository;

import com.catonetwork.accesscontrol.servicemodel.Identities;
import com.catonetwork.accesscontrol.servicemodel.Permissions;
import com.catonetwork.accesscontrol.servicemodel.Roles;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdentitiesRepository extends JpaRepository<Identities,Long> {

  List<Identities> findByUserId(String userId);

}
