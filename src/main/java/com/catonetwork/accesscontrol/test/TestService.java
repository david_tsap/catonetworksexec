package com.catonetwork.accesscontrol.test;

import com.catonetwork.accesscontrol.repository.IdentitiesRepository;
import com.catonetwork.accesscontrol.repository.IdentitiesRolesMapRepository;
import com.catonetwork.accesscontrol.repository.PermissionsRepository;
import com.catonetwork.accesscontrol.repository.PermissionsRolesMapRepository;
import com.catonetwork.accesscontrol.repository.RolesRepository;
import com.catonetwork.accesscontrol.servicemodel.Identities;
import com.catonetwork.accesscontrol.servicemodel.IdentitiesRolesMap;
import com.catonetwork.accesscontrol.servicemodel.Permissions;
import com.catonetwork.accesscontrol.servicemodel.PermissionsRolesMap;
import com.catonetwork.accesscontrol.servicemodel.Roles;
import java.util.Arrays;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TestService {

  @Autowired
  private IdentitiesRepository identitiesRepository;

  @Autowired
  private PermissionsRepository permissionsRepository;

  @Autowired
  private RolesRepository rolesRepository;

  @Autowired
  private IdentitiesRolesMapRepository identitiesRolesMapRepository;

  @Autowired
  private PermissionsRolesMapRepository permissionsRolesMapRepository;

  @Transactional
  public void initDbForUser(String userId) {
    Identities identities = new Identities();
    Roles roles = new Roles();
    Permissions permissions = new Permissions();
    PermissionsRolesMap permissionsRolesMap = new PermissionsRolesMap();
    IdentitiesRolesMap identitiesRolesMap = new IdentitiesRolesMap();

    permissions.setEndpoint("endpoint1");
    roles.setName("Admin");
    identities.setUserId(userId);

    permissionsRolesMap.setPermissions(permissions);
    permissionsRolesMap.setRoles(roles);

    identitiesRolesMap.setIdentities(identities);
    identitiesRolesMap.setRoles(roles);

    identitiesRepository.save(identities);
    permissionsRepository.save(permissions);
    rolesRepository.save(roles);
    identitiesRolesMapRepository.save(identitiesRolesMap);
    permissionsRolesMapRepository.save(permissionsRolesMap);
  }
}
