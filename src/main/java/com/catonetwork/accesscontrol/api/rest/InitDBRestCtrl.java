package com.catonetwork.accesscontrol.api.rest;


import com.catonetwork.accesscontrol.test.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/***
 * This rest Controller will be used only for test
 * It will create a dummy data in DB for testing
 *
 */
@RestController
public class InitDBRestCtrl {


  @Autowired
  private TestService testService;


  //this API only for testing
  @GetMapping(path = "/api/{userId}/init")
  public ResponseEntity<Void> initDb(@PathVariable String userId){
     testService.initDbForUser(userId);
    return new ResponseEntity<Void>(HttpStatus.OK);
  }
}
