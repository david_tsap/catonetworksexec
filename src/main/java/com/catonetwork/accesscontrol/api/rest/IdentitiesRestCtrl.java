package com.catonetwork.accesscontrol.api.rest;

import com.catonetwork.accesscontrol.service.IdentitiesService;
import com.catonetwork.accesscontrol.servicemodel.Permissions;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IdentitiesRestCtrl {

  @Autowired
  private IdentitiesService identitiesService;

  @GetMapping(path = "/api/{userId}/endpoints")
  public ResponseEntity<List<String>> userEndpoints(@RequestHeader("pass") String pass ,@PathVariable String userId)
      throws Exception {
    List<Permissions> userEndpoints = identitiesService.getUserEndpoints(userId, pass);
    List<String> collect = userEndpoints.stream().map(permissions -> permissions.getEndpoint())
        .collect(Collectors.toList());
    return new ResponseEntity<>(collect, HttpStatus.OK);
  }

}
