# Getting Started

### General 
Access control service is spring boot service with H2 embedded DB(SQL).  
The service will authenticate users and return a list of HTTP endpoints the user can access.  
port server is : 8080  
To change port, go to "application.properties" and change the server.port property.

### Api
GET: /api/{userId}/endpoints
  
You have to add a header "pass" for this API.  
please make sure that the pass is define in the configuration file for user.  
path to file /resources/static/configuration.json  
example:  
curl -sb -X GET -H "pass: 123" localhost:8080/api/CatoUser/endpoints

### Test API 
GET: /api/{userId}/init  

This API will create some dummy data in DB.  
It will create one identity with the userId in the URL.  
For this identity it will create a "Admin" role, and will create a permission
for Admin role to one endpoint call "endpoint1".
Code: TestService class.  
example:  
curl -sb -X GET localhost:8080/api/CatoUser/init

### authenticate users
As the service will start, we will read the configuration file located in resources/static/configuration.json
using globalConfiguration service.  
When we will call the /api/{userId}/endpoints API, we will take from the header the password for user
and will authenticate the user using the validateAuthenticationService. 

### Errors 
User authentication failed. CODE : IDN_10000

### compile
In root directory run:  
mvn clean install

### Run
After running compile using maven in target directory "accessControlService-0.0.1-SNAPSHOT.jar" will
be created.
go to target dir and run:  
java -jar accessControlService-0.0.1-SNAPSHOT.jar

### Basic flow
* cd to root directory
* run: mvn clean install
* cd to target directory.
* run: java -jar accessControlService-0.0.1-SNAPSHOT.jar
* run: curl -sb -X GET localhost:8080/api/CatoUser/init
* run: curl -sb -X GET -H "pass: 123" localhost:8080/api/CatoUser/endpoints
